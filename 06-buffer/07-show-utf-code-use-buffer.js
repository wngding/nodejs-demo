#!/usr/bin/env node

const dec2Hex = num => num === 0 ? '00': num.toString(16);

function utf8Code(str) {
  let arr = Array.from(Buffer.from(str, 'utf8'));
  return arr.map(dec2Hex).join('');
}

function utf16Code(str) {
  let arr = Array.from(Buffer.from(str, 'utf16le'));
  return arr.map(dec2Hex).join('');
}

function getCodeInfo(c) {
  return {
    'char':   c,
    'UTF-8':  utf8Code(c),
    'UTF-16': utf16Code(c),
  };
}

function main() {
  const msg = 'hello, 王顶';
  const codes = [...msg].map(getCodeInfo);
  console.table(codes);
}

main();
